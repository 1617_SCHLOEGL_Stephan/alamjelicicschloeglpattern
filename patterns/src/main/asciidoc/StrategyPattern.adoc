= Strategy Pattern

== UML Diagram



[plantuml,try-me,png]
----
@startuml
Klient --> Strategie
Klient--> Kontext
Kontext o-- Strategie
KonkreteStrategieB --> Strategie
KonkreteStrategieA --> Strategie

class KonkreteStrategieB {
   void Algorythmus()
}
 class KonkreteStrategieA {
   void Algorythmus()
}
class Kontext {
    void KontextSchnittstelle()
}
class Strategie {
    void Algorythmus()
}
@enduml
----



== Verwendung:
* Daten müssen ausgeführt werden
* mehrere Wege das zu tun (Strategien
* unterschiedliche Varianten eines Ablaufs werden benötigt
* während der Laufzeit wird entschieden welche Strategie benutzt werden soll

== Ziel
* austauschbaren Teil des Codes


 * Viele verwandte Klasen unterscheiden sich nur in ihrem Verhalten
 * unterschiedliche Varianten eines Ablaufs werden benötigt
 * Klasse muss flexiebler gestaltet werden

== Vorteile
 * große Gruppe von Alorithmen
 * Auswahl aus versch. Implementierungen ermöglicht --> höhere Flexibilität und Wiederverwendbarkeit

== Nachteile
 * User müssen verschiedene Strategien kennen, um sich für eine Entscheiden zu können
 * Kommunikationsaufwand zw. Strategie und Kontext
 * Anzahl der Objekte wird erhöht

== Beispiel
 * Image Load and Save Application
 * Save to pdf, png, jpg, ...

----
print "(c) Stpehan Schlögl";
